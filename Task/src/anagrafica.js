$(document).ready(function () {
    // Takes the users data from the JSON file
    $("#usersTable").DataTable({
        "ajax": ({
            url: "http://localhost:3000/anagrafica",
            method: "GET",
            dataSrc: "",
            dataType: "json"
        }),
  
        "columns": [
            { "data": "idanagrafica" },
            { "data": "RagioneSociale" },
            { "data": "Indirizzo" },
            { "data": "Citta" },
            { "data": "Prov" },
            { "data": "CAP" },
            { "data": "PIVA" },
            { "data": "CF" },
            { "data": "Telefono" },
            { "data": "Fax" },
            { "data": "Email" },
            {}, {}
        ],
     
        "columnDefs": [
            {
                "render": function () {
                    return "<button class= 'btn btn-info edit-button' >" + "Modifica" + "</button>";
                    
                },
                "targets": -2
            },
            {
                "render":function(){
                    return "<button class='btn btn-danger remove-button'>" + "Rimuovi" + "</button>";
                },
                "targets":-1
            },
        ]
    });

    //Aggiungi riga dal form
    $("#update-form").on('click', '.add-button', function () {
       let id = $("#id-FormInput").val();
       let rs = $("#rs-FormInput").val();
       let adr = $("#adr-FormInput").val();
       let city = $("#city-FormInput").val();
       let pr = $("#pr-FormInput").val();
       let cap = $("#cap-FormInput").val();
       let pv = $("#pv-FormInput").val();
       let cf = $("#cf-FormInput").val();
       let numb = $("#number-FormInput").val();
       let fax = $("#fax-FormInput").val();
       let email = $("#e-FormInput").val();

        $("#usersTable").prepend(
            "<tr>" +
            "<td>" + id + "</td>" +
            "<td>" + rs + "</td>" +
            "<td>" + adr + "</td>" +
            "<td>" + city + "</td>" +
            "<td>" + pr + "</td>" +
            "<td>" + cap + "</td>" +
            "<td>" + pv + "</td>" +
            "<td>" + cf + "</td>" +
            "<td>" + numb + "</td>" +
            "<td>" + fax + "</td>" +
            "<td>" + email + "</td>" +
            "<td>" + "<button class='btn btn-info edit-button'>Modifica</button>" + "</td>"+
            "<td>"+ "<button class='btn btn-danger remove-button'>Rimuovi</button>" + "</td>" +
            "</tr>"
        );
        
    });

    //Modifica
    $("#usersTable").on('click', '.edit-button', function () {
       let idCell = $(this).parents('tr').find('td:eq(0)').text();
       let rsCell = $(this).parents('tr').find('td:eq(1)').text();
       let adrCell = $(this).parents('tr').find('td:eq(2)').text();
       let cityCell = $(this).parents('tr').find('td:eq(3)').text();
       let prCell = $(this).parents('tr').find('td:eq(4)').text();
       let capCell = $(this).parents('tr').find('td:eq(5)').text();
       let pvCell = $(this).parents('tr').find('td:eq(6)').text();
       let cfCell = $(this).parents('tr').find('td:eq(7)').text();
       let numbCell = $(this).parents('tr').find('td:eq(8)').text();
       let faxCell = $(this).parents('tr').find('td:eq(9)').text();
       let emailCell = $(this).parents('tr').find('td:eq(10)').text();

     
        $(this).parents('tr').find('td:eq(0)').html("<input class='form-control form-control-sm' id='id-input' value='" + idCell + "'>");
        $(this).parents('tr').find('td:eq(1)').html("<input class='form-control form-control-sm' id='rs-input' value='" + rsCell + "'>");
        $(this).parents('tr').find('td:eq(2)').html("<input class='form-control form-control-sm' id='adr-input' value='" + adrCell + "'>");
        $(this).parents('tr').find('td:eq(3)').html("<input class='form-control form-control-sm' id='city-input' value='" + cityCell + "'>");
        $(this).parents('tr').find('td:eq(4)').html("<input class='form-control form-control-sm' id='pr-input' value='" + prCell + "'>");
        $(this).parents('tr').find('td:eq(5)').html("<input class='form-control form-control-sm' id='cap-input' value='" + capCell + "'>");
        $(this).parents('tr').find('td:eq(6)').html("<input class='form-control form-control-sm' id='pv-input' value='" + pvCell + "'>");
        $(this).parents('tr').find('td:eq(7)').html("<input class='form-control form-control-sm' id='cf-input' value='" + cfCell + "'>");
        $(this).parents('tr').find('td:eq(8)').html("<input class='form-control form-control-sm' id='numb-input' value='" + numbCell + "'>");
        $(this).parents('tr').find('td:eq(9)').html("<input class='form-control form-control-sm' id='fax-input' value='" + faxCell + "'>");
        $(this).parents('tr').find('td:eq(10)').html("<inpu class='form-control form-control-sm'  id='email-input' value='" + emailCell + "'>");

       
        $(this).parents('tr').find('td:eq(11)').append('<button class="btn btn-info update-button">Aggiorna</button>');
        $(this).hide();
    });


    //aggiorna modifiche
    $("#usersTable").on('click', '.update-button', function () {

       let idInput = $(this).parents('tr').find('#id-input').val();
       let rsInput = $(this).parents('tr').find('#rs-input').val();
       let adrInput = $(this).parents('tr').find('#adr-input').val();
       let cityInput = $(this).parents('tr').find('#city-input').val();
       let prInput = $(this).parents('tr').find('#pr-input').val();
       let capInput = $(this).parents('tr').find('#cap-input').val();
       let pvInput = $(this).parents('tr').find('#pv-input').val();
       let cfInput = $(this).parents('tr').find('#cf-input').val();
       let numbInput = $(this).parents('tr').find('#numb-input').val();
       let faxInput = $(this).parents('tr').find('#fax-input').val();
       let emailInput = $(this).parents('tr').find('#email-input').val();

        $(this).parents('tr').find('td:eq(0)').text(idInput);
        $(this).parents('tr').find('td:eq(1)').text(rsInput);
        $(this).parents('tr').find('td:eq(2)').text(adrInput);
        $(this).parents('tr').find('td:eq(3)').text(cityInput);
        $(this).parents('tr').find('td:eq(4)').text(prInput);
        $(this).parents('tr').find('td:eq(5)').text(capInput);
        $(this).parents('tr').find('td:eq(6)').text(pvInput);
        $(this).parents('tr').find('td:eq(7)').text(cfInput);
        $(this).parents('tr').find('td:eq(8)').text(numbInput);
        $(this).parents('tr').find('td:eq(9)').text(faxInput);
        $(this).parents('tr').find('td:eq(10)').text(emailInput);


        $(".edit-button").show();
        $('.update-button').hide();

    });

    //Delete row
    $("#usersTable").on('click', '.remove-button', function () {
        $(this).closest('tr').remove();
    });

    //Nascondi Form
    $('.showform').ready(function() {
        $("#update-form").hide();
        $("#nascondi").hide();

        $("#nascondi").click(function(){
        $("#update-form").hide();
        $("#mostra").show();
        $("#nascondi").hide();

        });
        
        $("#mostra").click(function(){
            //toogle buttom add-row             
        $("#update-form").animate({
            height: 'toggle'});
        $("#update-form").show();
        $("#nascondi").show();
        $("#mostra").hide();
        });
       });
});